import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody=()=>{
      return  this.props.cart.map((shoe)=>{
           return <tr>
             <td>{shoe.id}</td>
             <td>{shoe.name}</td>
             <td>
             <button className='btn btn-danger' onClick={()=>{
             this.props.handleChangeQuantity(shoe.id,-1)
            }}

      style={{width:20,height:30}} >-</button>
             <strong>{shoe.soLuong}</strong>
             <button className='btn btn-success'
              onClick={()=>{
                this.props.handleChangeQuantity(shoe.id,1)
               }}
             style={{width:20,height:30}}>+</button>
             </td>
             <td>{shoe.price * shoe.soLuong}$</td>
             <td><img src={shoe.image} alt="" style={{width:80}} /></td>
             <td><button className='btn btn-warning' onClick={()=>{
              this.props.handleRemoveShoe(shoe.id)
             }}>Xóa</button></td>
         </tr>
     })
    }
   
  render() {
    return (
      <div>
  {/* Modal */}
  <div className="modal fade" id="exampleModalCenter" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLongTitle"><i class="fa fa-store"></i> ShoeShop</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">
        <table className="table">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Img</th>
                <th>Thao tác</th>
            </thead>
            <tbody>
                {this.renderTbody()}
            </tbody>
        </table>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary">Thanh Toán</button>
        </div>
      </div>
    </div>
  </div>




      </div>
    )
  }
}
