import React, { Component } from 'react'

export default class ItemShoes extends Component {

  render() {
    let {image,name,price} = this.props.item;
    return (
      <div className='col-3 p-3'>
   <div className="card" >
  <img className="card-img-top" src={image} alt="Card image cap" />
  <div className="card-body">
    <h5 className="card-title">{name}</h5>
    <p className="card-text">{price}$</p>
    <a href="#" className="btn btn-secondary"  onClick={()=>{
          this.props.handleOnclick(this.props.item)
    }} >Add To Cart </a>
  </div>
</div>

    </div>
    )
  }
}
