import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './Data_Shoes'
import ListShoe from './ListShoe'

export default class Ex_ShoesShop extends Component {
  state={
    listShoe : dataShoe,
    cart:[],
    soLuong:"",
  };
  handleAddToCart =(shoe)=>{
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item)=>{
         return item.id == shoe.id;
    })
    if(index == -1){
      let newShoe={...shoe,soLuong:1};
      cloneCart.push(newShoe);
    }else{
    cloneCart[index].soLuong++;
    }
this.setState({
 cart:cloneCart,
})
  }

  handleChangeQuantity =(id,num)=>{

 let index = this.state.cart.findIndex((item)=>{
  return id == item.id;
 })
  this.state.cart[index].soLuong+=num*1;
  this.setState({
    soLuong:this.state.cart[index].soLuong*1
  })
  if(this.state.cart[index].soLuong ==0){
    this.state.cart.splice(index,1)
  }
  }

  handleRemoveShoe = (id)=>{
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item)=>{
         return item.id == id;
    })
cloneCart.splice(index,1)
this.setState({
  cart:cloneCart
})
  }
  render() {

    return (
      <div className='container' >
        <h1 className='text-secondary' >ShoeShop</h1>
        <a href="" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-shopping-cart" ></i></a>
        <Cart cart = {this.state.cart} handleChangeQuantity={this.handleChangeQuantity} handleRemoveShoe={this.handleRemoveShoe}/>
        <ListShoe handleAddToCart = {this.handleAddToCart}  list = {this.state.listShoe}  />
      </div>
    )
  }
}
