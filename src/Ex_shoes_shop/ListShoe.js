import React, { Component } from 'react'
import ItemShoes from './ItemShoes'

export default class ListShoe extends Component {
  render() {
  
    return (
      <div className='row'>
  {this.props.list.map((shoe)=>{
    return <ItemShoes 
    handleOnclick={this.props.handleAddToCart}
    
    item = {shoe}
    />
  })}
      </div>
    )
  }
}
